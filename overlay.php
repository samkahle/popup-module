<?php $dynamic_node = blueprintoverlay_loadnodebasedonpath(); ?>

<?php if ($dynamic_node): ?>

<script type="text/javascript" src="/sites/all/modules/bp_popup_module/popup-styles/popup.js"></script>

<?php if($dynamic_node->field_popup_style['und'][0]['value'] == "Bent White"): ?>

    <link href="/sites/all/modules/bp_popup_module/popup-styles/style1.css" rel="stylesheet">

<?php elseif($dynamic_node->field_popup_style['und'][0]['value'] == "Black"): ?>

    <link href="/sites/all/modules/bp_popup_module/popup-styles/style2.css" rel="stylesheet">

<?php elseif($dynamic_node->field_popup_style['und'][0]['value'] == "Sidebar"): ?>

    <link href="/sites/all/modules/bp_popup_module/popup-styles/style3.css" rel="stylesheet">

<?php elseif($dynamic_node->field_popup_style['und'][0]['value'] == "Obsidian"): ?>

    <link href="/sites/all/modules/bp_popup_module/popup-styles/style4.css" rel="stylesheet">

<?php elseif($dynamic_node->field_popup_style['und'][0]['value'] == "Basic"): ?>

    <link href="/sites/all/modules/bp_popup_module/popup-styles/style5.css" rel="stylesheet">

<?php endif ?>

<?php if(isset($dynamic_node->field_expiration_date['und'][0]['value'])): ?>

    <?php $expiration1 = strtotime($dynamic_node->field_expiration_date['und'][0]['value']); ?>
    <?php $time1 = time(); ?>
    <?php if($time1 < $expiration1): ?>

	<?php
		if (isset($dynamic_node->field_image) && !empty($dynamic_node->field_image['und'][0]['filename'])) {
		  	$filename = $dynamic_node->field_image['und'][0]['uri'];
		    $field_image = file_create_url($filename);
		} else {
		  $field_image = "";
		}
    ?>


<?php if(isset($dynamic_node->field_cookie_after_close['und'][0]['value'])  && $dynamic_node->field_cookie_after_close['und'][0]['value'] == 1): ?>

<?php
function create_slug($string){
    $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $string);
    return $slug;
 }
?>
    <div id="dvGlobalMask"></div>

    <div id="popupc" class="popupc animated bounceIn cookiepopup" data-popup="<?php print create_slug($dynamic_node->title); ?>">

<?php else: ?>

    <div id="dvGlobalMask"></div>

    <div id="popupc" class="popupc animated bounceIn notcookiepopup">

<?php endif ?>


    <div class="popupcclose">
        <svg width="30px" height="30px" viewBox="0 0 30 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <title>Combined Shape</title>
            <desc>Created with Sketch.</desc>
            <g id="Designs" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <g id="BLU1902_Page-3_Pop-Up-3_Desktop" transform="translate(-1184.000000, -47.000000)" fill="#FFFFFF" fill-rule="nonzero">
                    <g id="Group-4" transform="translate(338.000000, -2.000000)">
                        <path d="M865.041631,60.0416306 L878.041631,60.0416306 L878.041631,68.0416306 L865.041631,68.0416306 L865.041631,81.0416306 L857.041631,81.0416306 L857.041631,68.0416306 L844.041631,68.0416306 L844.041631,60.0416306 L857.041631,60.0416306 L857.041631,47.0416306 L865.041631,47.0416306 L865.041631,60.0416306 Z" id="Combined-Shape" transform="translate(861.041631, 64.041631) rotate(45.000000) translate(-861.041631, -64.041631) "></path>
                    </g>
                </g>
            </g>
        </svg>
    </div>
    <div class="popup-container">

        <?php if(isset($dynamic_node->field_video_embed_id['und'][0]['value'])): ?>


        <div class="resp-container">
            <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/<?php print render($dynamic_node->field_video_embed_id['und'][0]['value']); ?>?modestbranding=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <?php else: ?>

         <?php

                $popupimg = $dynamic_node->field_popup_image['und'][0]['uri'];
                $popupimg = image_style_url('popup_image', $popupimg);
        ?>

        <img src="<?php print $popupimg; ?>" class="popupimg">
        <?php endif ?>




        <div class="popupctext">

            <h2><?php print render($dynamic_node->title); ?></h2>
            <?php print render($dynamic_node->body['und'][0]['value']); ?>

            <?php if($dynamic_node->field_popup_content['und'][0]['value'] == "Take Action Button"): ?>
            <a class="tabtn" href="<?php print render($dynamic_node->field_button_url['und'][0]['value']); ?>" target="_blank"><?php print render($dynamic_node->field_button_text['und'][0]['value']); ?></a>
            <?php endif ?>
        </div>

        <?php if($dynamic_node->field_popup_content['und'][0]['value'] == "Donate Buttons"): ?>
            <?php if(isset($dynamic_node->field_progress_bar_percentage['und'][0]['value'])): ?>

            <?php
                $achieved = $dynamic_node->field_progress_bar_percentage['und'][0]['value'];
                $goal = $dynamic_node->field_goal['und'][0]['value'];
                $percentage = ($achieved/$goal) * 100;
            ?>

            <br>
            <div class="progressbar">
                <div class="progress" style="width:<?php print $percentage; ?>%">
                    <h3 class="remaining">$<?php print render($dynamic_node->field_progress_bar_percentage['und'][0]['value']); ?></h3>
                </div>
            </div>
            <!-- <h3 class="goal">$<//?php print render($dynamic_node->field_goal['und'][0]['value']); ?></h3> -->

            <?php endif ?>

        <div class="menu-donate-links">
            <?php
            $view_mode = 'full';
            $node = node_load($dynamic_node->nid);
            $view = node_view($node, $view_mode);
            print render($view['field_donate_buttons']);
            ?>
        </div>

        <?php endif ?>


        <?php if($dynamic_node->field_popup_content['und'][0]['value'] == "Form"): ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

            <div class="popup-form">
                <?php if(isset($dynamic_node->field_redirect_url['und'][0]['value']) && !empty($dynamic_node->field_redirect_url['und'][0]['value'])): ?>
                    <form method="POST" action="/signup/process">
                <?php else: ?>
                    <form>
                <?php endif ?>
                    <input name="email" type="email" placeholder="E-mail" required>
                    <input name="phonenumber" type="tel" placeholder="Mobile #">
                    <input name="zip" type="tel" placeholder="ZIP Code" required>
                    <?php if(isset($dynamic_node->field_action_kit_id['und'][0]['value']) && !empty($dynamic_node->field_action_kit_id['und'][0]['value'])): ?>
                        <input type="hidden" name="action-kit-id" value="<?php print render($dynamic_node->field_action_kit_id['und'][0]['value']); ?>">
                    <?php else: ?>
                        <input type="hidden" name="action-kit-id" value="indivisible-website-sign">
                    <?php endif ?>
                    <?php if(isset($dynamic_node->field_redirect_url['und'][0]['value'])): ?>
                        <input type="hidden" name="redirecturl" value="<?php print render($dynamic_node->field_redirect_url['und'][0]['value']); ?>">
                    <?php endif ?>
                    <input type="submit" value="Sign Up">
                </form>
                <p class="disclaimerp"><?php print render($dynamic_node->field_disclaimer['und'][0]['value']); ?></p>
                <h2 class="thanksfam" style="display:none">Thanks for signing up!</h2>


            <?php if(!isset($dynamic_node->field_redirect_url['und'][0]['value']) && empty($dynamic_node->field_redirect_url['und'][0]['value'])): ?>
                <script>

                    jQuery(".popup-form form").submit(function(blah){
                        blah.preventDefault();
                        jQuery('.popup-form form').fadeOut(800);
                        jQuery('.popup-form .thanksfam').delay(800).fadeIn();
                        jQuery.ajax({
                            method:"POST",
                            url: "/signup/process",
                            data: jQuery(".popup-form form").serialize(),
                        }).done(function(response){
                            console.log(jQuery(".popup-form form").serialize());
                        });
                    });

                </script>
            <?php endif ?>

            </div>
        <?php endif ?>

    </div>

</div>


<?php endif ?>


<?php else: ?>

	<?php
		if (isset($dynamic_node->field_image) && !empty($dynamic_node->field_image['und'][0]['filename'])) {
		  	$filename = $dynamic_node->field_image['und'][0]['uri'];
		    $field_image = file_create_url($filename);
		} else {
		  $field_image = "";
		}
	?>

<div id="dvGlobalMask"></div>

<div id="popupc" class="popupc animated bounceIn">

    <div class="popupcclose">
        <img src="/sites/all/themes/ind/img/close.png">
    </div>
    <div class="popup-container">

        <?php if(isset($dynamic_node->field_video_embed_code['und'][0]['value'])): ?>
        <div class="resp-container">
            <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/<?php print render($dynamic_node->field_video_embed_code['und'][0]['value']); ?>?modestbranding=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>

        <?php else: ?>

            <?php
            $popupimg = $dynamic_node->field_popup_image['und'][0]['uri'];
            $popupimg = image_style_url('popup_image', $popupimg);
            ?>

        <img src="<?php print $popupimg; ?>" class="popupimg">

        <?php endif ?>


        <div class="popupctext">
            <h2><?php print render($dynamic_node->title); ?></h2>

            <?php print render($dynamic_node->body['und'][0]['value']); ?>

            <?php if($dynamic_node->field_popup_feature['und'][0]['value'] == "Take Action Button"): ?>
            <a class="tabtn" href="<?php print render($dynamic_node->field_column_3_button_link['und'][0]['value']); ?>" target="_blank"><?php print render($dynamic_node->field_left_button_text['und'][0]['value']); ?></a>
            <?php endif ?>
        </div>


        <?php if($dynamic_node->field_popup_content['und'][0]['value'] == "Donate Buttons"): ?>
        <?php if(isset($dynamic_node->field_progress_bar_percentage['und'][0]['value'])): ?>

        <?php
            $achieved = $dynamic_node->field_progress_bar_percentage['und'][0]['value'];
            $goal = $dynamic_node->field_goal['und'][0]['value'];
            $percentage = ($achieved/$goal) * 100;
        ?>

        <div class="progressbar">
            <div class="progress" style="width:<?php print $percentage; ?>%">
                <h3 class="remaining">$<?php print render($dynamic_node->field_progress_bar_percentage['und'][0]['value']); ?></h3>
            </div>
        </div>
        <h3 class="goal">$<?php print render($dynamic_node->field_goal['und'][0]['value']); ?></h3>

        <?php endif ?>

        <div class="menu-donate-links">
            <?php
            $view_mode = 'full';
            $node = node_load($dynamic_node->nid);
            $view = node_view($node, $view_mode);
            print render($view['field_donate_buttons']);
            ?>
        </div>
        <?php endif ?>

        <?php if($dynamic_node->field_popup_content['und'][0]['value'] == "Form"): ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

            <div class="popup-form">
                <?php if(isset($dynamic_node->field_redirect_url['und'][0]['value']) && !empty($dynamic_node->field_redirect_url['und'][0]['value'])): ?>
                    <form method="POST" action="/signup/process">
                <?php else: ?>
                    <form>
                <?php endif ?>
                    <input name="email" type="email" placeholder="E-mail" required>
                    <input name="phonenumber" type="tel" placeholder="Mobile #">
                    <input name="zip" type="tel" placeholder="ZIP Code" required>
                    <?php if(isset($dynamic_node->field_action_kit_id['und'][0]['value']) && !empty($dynamic_node->field_action_kit_id['und'][0]['value'])): ?>
                        <input type="hidden" name="action-kit-id" value="<?php print render($dynamic_node->field_action_kit_id['und'][0]['value']); ?>">
                    <?php else: ?>
                        <input type="hidden" name="action-kit-id" value="indivisible-website-sign">
                    <?php endif ?>
                    <?php if(isset($dynamic_node->field_redirect_url['und'][0]['value'])): ?>
                        <input type="hidden" name="redirecturl" value="<?php print render($dynamic_node->field_redirect_url['und'][0]['value']); ?>">
                    <?php endif ?>
                    <input type="submit" value="Sign Up">
                </form>
                <p class="disclaimerp"><?php print render($dynamic_node->field_disclaimer['und'][0]['value']); ?></p>
                <h2 class="thanksfam" style="display:none">Thanks for signing up!</h2>
            <?php if(!isset($dynamic_node->field_redirect_url['und'][0]['value']) && empty($dynamic_node->field_redirect_url['und'][0]['value'])): ?>
                <script>

                    jQuery(".popup-form form").submit(function(blah){
                        blah.preventDefault();
                        jQuery('.popup-form form').fadeOut(800);
                        jQuery('.popup-form .thanksfam').delay(800).fadeIn();
                        jQuery.ajax({
                            method:"POST",
                            url: "/signup/process",
                            data: jQuery(".popup-form form").serialize(),
                        }).done(function(response){
                            console.log(jQuery(".popup-form form").serialize());
                        });
                    });

                </script>
            <?php endif ?>
            </div>
        <?php endif ?>

    </div>

</div>

<?php endif ?>

<?php endif ?>