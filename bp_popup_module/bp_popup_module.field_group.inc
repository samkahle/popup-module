<?php
/**
 * @file
 * bp_popup_module.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function bp_popup_module_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_btns|node|popup|form';
  $field_group->group_name = 'group_btns';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'popup';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_content';
  $field_group->data = array(
    'label' => 'Donate Buttons',
    'weight' => '21',
    'children' => array(
      0 => 'field_donate_buttons',
      1 => 'field_progress_bar_percentage',
      2 => 'field_goal',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-btns field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_btns|node|popup|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_content|node|popup|form';
  $field_group->group_name = 'group_content';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'popup';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Content',
    'weight' => '15',
    'children' => array(
      0 => 'group_form',
      1 => 'group_btns',
      2 => 'group_ta',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_content|node|popup|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cookie|node|popup|form';
  $field_group->group_name = 'group_cookie';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'popup';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_display_options';
  $field_group->data = array(
    'label' => 'Cookie Options',
    'weight' => '55',
    'children' => array(
      0 => 'field_generate_cookie',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-cookie field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_cookie|node|popup|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_display_options|node|popup|form';
  $field_group->group_name = 'group_display_options';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'popup';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Display Options',
    'weight' => '16',
    'children' => array(
      0 => 'group_horizontal_tab',
      1 => 'group_expiration_date',
      2 => 'group_cookie',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_display_options|node|popup|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_expiration_date|node|popup|form';
  $field_group->group_name = 'group_expiration_date';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'popup';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_display_options';
  $field_group->data = array(
    'label' => 'Expiration Date',
    'weight' => '54',
    'children' => array(
      0 => 'field_expiration_date',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-expiration-date field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_expiration_date|node|popup|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_horizontal_tab|node|popup|form';
  $field_group->group_name = 'group_horizontal_tab';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'popup';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_display_options';
  $field_group->data = array(
    'label' => 'Horizontal Tab',
    'weight' => '53',
    'children' => array(
      0 => 'field_display_individual',
      1 => 'field_display_paths',
      2 => 'field_hide_individual',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_horizontal_tab|node|popup|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ta|node|popup|form';
  $field_group->group_name = 'group_ta';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'popup';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_content';
  $field_group->data = array(
    'label' => 'Take Action',
    'weight' => '20',
    'children' => array(
      0 => 'field_button_text',
      1 => 'field_button_url',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-ta field-group-htab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_ta|node|popup|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Cookie Options');
  t('Display Options');
  t('Donate Buttons');
  t('Expiration Date');
  t('Horizontal Tab');
  t('Take Action');

  return $field_groups;
}
