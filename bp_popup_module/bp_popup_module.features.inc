<?php
/**
 * @file
 * bp_popup_module.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function bp_popup_module_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function bp_popup_module_node_info() {
  $items = array(
    'popup' => array(
      'name' => t('Popup'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
